# Docker NFS Storage Plugin

This is an example of how to use the Docker NFS storage plugin with Docker Swarm and `docker-compose`.

## Gotchas
Pay attention to the options.  Be aware of what `soft` and `nolock` are before you wipe my settings and utilize what is in the Stack Overflow post.  I'll be doing testing at a later date in order to determine the correct flags for my defployment.

You MUST use compose v3.2 or later.

## Credit

Ref: https://stackoverflow.com/a/46481637/10195252
